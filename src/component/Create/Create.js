import React, { Component } from 'react';
import './Create.css';

class Create extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
      }
      handleClick() {
      
        console.log("clicked")
        
      }

      render(){
        return(
            <div>
                   
                <div className="Form">
                    <div className="CardMaker">
                        <h3>Fill Data</h3>
                        <label>Title</label>
                        <input className="Title" type="text" />
                        <textarea name="description" defaultValue="This is a description." rows="4" cols="50" />
                    </div>
                    <div className="Buttons">
                        <button  className="Buttons" onClick={this.handleClick}>Create</button>
                    </div>
                </div>
                
            </div>
        )
    
      }
};
export default Create;