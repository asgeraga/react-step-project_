import React, {Component} from 'react';
import Card from "../Card/Card";
class Home extends Component {
    render() {
        return (
            <div className="Card">
                <div  className="CardInner" >
                    <Card name= "Note title" text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "  />
                </div>
            </div>
        );
    }
}
export default Home;