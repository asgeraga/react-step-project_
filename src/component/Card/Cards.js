import React from 'react';
import {Link} from 'react-router-dom'
const Cards = ({title, text}) => {
    return(
        <div className="CardsDiv">
            <Link to="/singlenote">
          <p class="cardsTitle">
              {title}
          </p>
            </Link>
            <p>
                {text}
            </p>
        </div>
    );
};
export default Cards;