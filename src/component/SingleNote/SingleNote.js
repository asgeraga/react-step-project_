import React, { Component }  from 'react';
import './SingleNote.css';
import ModalWindow from "../ModalWindow/ModalWindow";
import Button from '../Header/Button';




class SingleNote extends Component{
    state = {
        isOpen: false
    }
    handleClick() {
      
        console.log("clicked")
        
      }
    render() {
        return (
            <div className="singleContainer">


                
                <div className="CardMaker">
                    <h3>NOTE</h3>
                    <label>Title</label>
                    <input className="Title" type="text" />
                    <label>Description</label>
                    <textarea name="description" defaultValue="This is a description." rows="4" cols="50" />              
                </div>
                
                <div className="singleDiv">
                    <div>
                    <button className="Buttons" onClick={this.handleClick} >Edit</button>

                    </div>
                    <div>
                    <button  className="Buttons"onClick={this.handleClick} >Archive</button>

                    </div>
                    <div>
                        <button onClick={(e) => this.setState({isOpen: true})} >DELETE</button>
                    </div>


                    <ModalWindow isOpen= {this.state.isOpen} onClose={ (e) => this.setState({isOpen: false})}>

                    </ModalWindow>
                    <div>{this.props.children}</div>


                </div>

            </div>
        );
    }
}






export default SingleNote;