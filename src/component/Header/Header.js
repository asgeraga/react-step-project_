import React from 'react';
import './Header.css';
import {Link} from 'react-router-dom'
const Header = (props) => {

    return( 
        <div>
            <nav>
                <h4>NotesApp</h4>

                <ul className="navLink" >
                    <Link to="/actual">
                        <button>Actual</button>
                    </Link>

                    <Link  to="/">
                        <button>Archive</button>
                    </Link>

                    <Link  to="/create">
                        <button>Create</button>
                    </Link>
                </ul>
            </nav>

        </div>
     )
};
export default Header;