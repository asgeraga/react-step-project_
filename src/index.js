import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router,Switch, Route } from 'react-router-dom';

import './index.css';
import Create from './component/Create/Create';
import SingleNote from './component/SingleNote/SingleNote';

import Home from "./component/Home/Home";
import Header from "./component/Header/Header";


import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>


      <Router>
          <div >
              <Header />
              <Switch>
                  <Route path="/actual" exact component={Home} />
                  <Route path="/create" component={Create} />

                  <Route path="/singlenote"  component={SingleNote} />
              </Switch>

          </div>
      </Router>

   



  



  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
